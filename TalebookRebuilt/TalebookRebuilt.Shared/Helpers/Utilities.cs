﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Windows.UI;

namespace TalebookRebuilt.Helpers
{
    public static class Utilities
    {
        /// <summary>
        /// Converts either a hexadecimal or English name string representation of a color to a Color object.
        /// </summary>
        /// <param name="colorString">The string representing the color, either in hexadecimal aRGB or as an English word.</param>
        /// <returns>The appropriate Windows.UI.Color object, if a valid color string is passed in. Returns Colors.Gray otherwise.</returns>
        public static Color StringToColor(string colorString)
        {                        
            Color color = Colors.Gray;
            var property = typeof(Colors).GetRuntimeProperty(colorString);
            if (property != null)
            {
                color = (Color)property.GetValue(null);                
            }
            else //Handle hex strings
            {
                colorString = colorString.Trim();
                colorString = colorString.TrimStart('#');                
                if(IsHex(colorString))           
                {                    
                    //aRGB hex string
                    if (colorString.Length == 8)
                    {
                        byte a = byte.Parse(colorString.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                        byte r = byte.Parse(colorString.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
                        byte g = byte.Parse(colorString.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
                        byte b = byte.Parse(colorString.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);                        
                    }
                    //No alpha, regular RGB hex string
                    else if (colorString.Length == 6)
                    {
                        byte a = byte.MaxValue;
                        byte r = byte.Parse(colorString.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
                        byte g = byte.Parse(colorString.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
                        byte b = byte.Parse(colorString.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
                        color = Color.FromArgb(a, r, g, b);
                    }

                }
            }
            return color;
        }

        public static bool IsHex(IEnumerable<char> str)
        {
            bool isHex;
            foreach(var c in str)
            {
                isHex = ((c >= '0' && c <= '9') ||
                         (c >= 'a' && c <= 'f') ||
                         (c >= 'A' && c <= 'F'));
                if (!isHex)
                    return false;
            }
            return true;
        }
    }
}
